<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Ravnica all character</title>
</head>
<body>
	<h3>${character.name}</h3>
    <p> Classe : ${character.classe}</p>
    <p> Race : ${character.race}</p>
    <p> Guilde : ${character.guilde}</p>
    <p> Niveau : ${character.niveau}</p>
</body>
</html>