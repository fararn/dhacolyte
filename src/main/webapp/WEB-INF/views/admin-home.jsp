<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <link href="<c:url value="/resources/css/main.css" />" rel="stylesheet">
    <%--<script type='text/javascript' src="<c:url value="/resources/js/app.js" />"></script> --%>

</head>

<body>

<div>

    <h1> Bonjour Pacte des guildes vivant<b><c:out value="${user}"/></b></h1>
    
   
   	<h2> Ajout d'un nouveau personnage</h2> 
   	
     <form method="get" action="api/addCharacter">
        <label for="name"> Nom : <span class="requis">*</span></label>
        <input type="text" id="name" name="name" value="" size="20" maxlength="60" />
        <br />

        <label for="classe"> Classe : <span class="requis">*</span></label>
        <input type="text" id="classe" name="classe" value="" size="20" maxlength="60" />
        <br />

        <label for="race"> Race : <span class="requis">*</span></label>
        <input type="text" id="race" name="race" value="" size="20" maxlength="60" />
        <br />

        <label for="guilde"> Guilde : <span class="requis">*</span></label>
        <input type="text" id="guilde" name="guilde" value="" size="20" maxlength="60" />
        <br />
        
        <label for="niveau"> Niveau : <span class="requis">*</span></label>
        <input type="text" id="niveau" name="niveau" value="" size="20" maxlength="60" />
        <br />

        <input type="submit" value="createCharacter" class="sansLabel" />
        <br />
    </form>
    
    
    <h2>Supprimer un aventurier</h2>
    
    <form method="get" action="api/deleteCharacter">
    
    	<label for="id"> ID : <span class="requis">*</span></label>
        <input type="text" id="id" name="id" value="" size="20" maxlength="60" />
        <br />

        <input type="submit" value="deleteCharacter" class="sansLabel" />
        <br />
    
    </form>
    
    
    <h2>Mettre à jour un aventurier</h2>
    
    <form method="get" action="api/updateCharacter">
    
        <label for="id"> Id : <span class="requis">*</span></label>
        <input type="text" id="id" name="id" value="" size="20" maxlength="60" />
        <br />
    
        <label for="name"> Nom : </label>
        <input type="text" id="name" name="name" value="" size="20" maxlength="60" />
        <br />

        <label for="classe"> Classe : </label>
        <input type="text" id="classe" name="classe" value="" size="20" maxlength="60" />
        <br />

        <label for="race"> Race : </label>
        <input type="text" id="race" name="race" value="" size="20" maxlength="60" />
        <br />

        <label for="guilde"> Guilde : </label>
        <input type="text" id="guilde" name="guilde" value="" size="20" maxlength="60" />
        <br />
        
        <label for="niveau"> Niveau : </label>
        <input type="text" id="niveau" name="niveau" value="" size="20" maxlength="60" />
        <br />

        <input type="submit" value="updateCharacter" class="sansLabel" />
        <br />
    </form>

    <a href="<c:url value="/logout" />">Logout</a>

</div>

</body>

</html>



