package fr.univ_smb.isc.m2.domain.team;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import fr.univ_smb.isc.m2.domain.character.Character;

@Entity
public class Team implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3974334722528841944L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int id;

	private String name;
	
	private List<Character> membreTeam;

	private int niveauMoyen;
	
	public Team() {
		super();
	}

	public Team(String teamName, List<Character> memberTeam, int niveauMoyen) {
		super();
		this.name = teamName; 
		this.niveauMoyen = niveauMoyen;
		this.membreTeam = (memberTeam == null)?new ArrayList<Character>():memberTeam;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Character> getMembreTeam() {
		return membreTeam;
	}

	public void setMembreTeam(List<Character> membreTeam) {
		this.membreTeam = membreTeam;
	}

	public int getNiveauMoyen() {
		return niveauMoyen;
	}

	public void setNiveauMoyen(int niveauMoyen) {
		this.niveauMoyen = niveauMoyen;
	}

	
	

}
